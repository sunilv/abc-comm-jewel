/* eslint-disable no-new */
/* eslint-disable class-methods-use-this */
import decode from 'jwt-decode';
import { Urls } from '../constants';

export default class ApiService {
  // Initializing URLS
  constructor() {
    this.domain = Urls.BASE_URL;
    this.account = Urls.ACCOUNT_URL;

    this.fetch = this.fetch.bind(this);
  }

  // Get Product List
  getProducts = (page, perPage, filter) => {
    const params = filter.length
      ? `page=${page}&per_page=${perPage}&filter=${filter}`
      : `page=${page}&per_page=${perPage}`;
    return this.fetch(`${this.domain}${Urls.URLS.products}?${params}`).then((res) => {
      return Promise.resolve(res);
    });
  };

  // Upload Product List
  uploadProducts = (products) => {
    return this.fetch(`${this.domain}${Urls.URLS.products}`, {
      method: 'POST',
      body: JSON.stringify({
        products,
      }),
    }).then((res) => {
      return Promise.resolve(res);
    });
  };

  // Add to wish List
  addToWishlist = (productId) => {
    return this.fetch(`${this.domain}${Urls.URLS.wishlists}`, {
      method: 'POST',
      body: JSON.stringify({
        product: productId,
      }),
    }).then((res) => {
      return Promise.resolve(res);
    });
  };

  // Get Wish List
  getWishlist = (page, perPage) => {
    return this.fetch(`${this.domain}${Urls.URLS.wishlists}?page=${page}&per_page=${perPage}`).then(
      (res) => {
        return Promise.resolve(res);
      },
    );
  };

  // Get Categories
  getCategories = (page, perPage) => {
    return this.fetch(
      `${this.domain}${Urls.URLS.categories}?page=${page}&per_page=${perPage}`,
    ).then((res) => {
      return Promise.resolve(res);
    });
  };

  // Remove Wishlist
  removeWishList = (wishId) => {
    return this.fetch(`${this.domain}${Urls.URLS.wishlists}`, {
      method: 'DELETE',
      body: JSON.stringify({
        wish: wishId,
      }),
    }).then((res) => {
      return Promise.resolve(res);
    });
  };

  // Get Product and category Counts
  getDashboard = () => {
    return this.fetch(`${this.domain}${Urls.URLS.dashboard}`).then((res) => {
      return Promise.resolve(res);
    });
  };

  // Verify User Token. Todo
  tokenVerify = (token) => {
    // Get a token from api server using the fetch api
    return this.fetch(`${this.domain}${Urls.URLS.tokenVerify}`, {
      method: 'POST',
      body: JSON.stringify({
        token,
      }),
    }).then((res) => {
      return Promise.resolve(res);
    });
  };

  // Login
  login = (mobile, password) => {
    console.log('jjjjjs');
    // Get a token from api server using the fetch api
    return this.fetch(`${this.account}${Urls.URLS.login}`, {
      method: 'POST',
      body: JSON.stringify({
        mobile,
        password,
      }),
    }).then((res) => {
      if (res.code === 200) {
        this.setToken(res.data); // Setting the token in localStorage
      }
      return Promise.resolve(res);
    });
  };

  // Login
  loggedIn = () => {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken(); // GEtting token from localstorage
    return !!token && !this.isTokenExpired(token); // handwaiving here
  };

  isTokenExpired = (token) => {
    try {
      this.decoded = decode(token);
      console.log(this.decoded);
      if (this.decoded.exp < Date.now() / 1000) {
        // Checking if token is expired. N
        return true;
      }
      return false;
    } catch (err) {
      return false;
    }
  };

  setToken = (data) => {
    // Saves user token to localStorage
    localStorage.setItem('ut', data.token);
    localStorage.setItem('nm', data.name);
    localStorage.setItem('im', data.image);
    localStorage.setItem('rl', data.role);
  };

  getToken = () => {
    // Retrieves the user token from localStorage
    return localStorage.getItem('ut');
  };

  getRole() {
    // Retrieves the user role from localStorage
    return localStorage.getItem('rl');
  }

  logout = () => {
    // Clear user token and profile data from localStorage
    localStorage.removeItem('ut');
  };

  getProfile = () => {
    // Using jwt-decode npm package to decode the token
    return decode(this.getToken());
  };

  fetch = (url, options) => {
    // performs api calls sending the required authentication headers
    let headers = {};
    if (!this.isFileUpload) {
      headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      };
    }

    // Setting Authorization header
    // Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx
    if (this.loggedIn()) {
      headers.Authorization = `Bearer ${this.getToken()}`;
    }

    return (
      fetch(url, {
        headers,
        ...options,
      })
        // .then(this.checkStatus)
        .then((response) => response.json())
    );
  };

  // eslint-disable-next-line consistent-return
  checkStatus = (response) => {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      // Success status lies between 200 to 300
      return response;
    }
    console.log(response);
    // eslint-disable-next-line prefer-const
    // let error = new Error(response.statusText);

    // return response;
    return Promise.reject(response);
    // error.response = response;
    // throw error;
  };
}
