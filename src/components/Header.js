import React, { useRef, useState } from 'react';
import { Box, Text, Anchor, Drop, Header as Head, Layer } from 'grommet';
import { Grid, Heart } from 'react-feather';
import { COLORS } from '../styles';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import WishLayout from '../components/WishLayout';

const FixedHeader = styled(Head)`
  position: absolute;
  top: 0;
  width: 100%;
`;

const ToolTip = styled(Box)`
  position: relative;
  color: white;
  span {
    position: absolute;
    top: -5px;
    left: 70px;
    width: 5px;
    padding: 5px;
    z-index: 100;
    background: ${COLORS.primary};
    color: #fff;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
  }
`;

/**
 * Header with Nav links
 */

function Header({ wishlist, toolTipMsg, removeWishList }) {
  const ref = useRef();
  const [showLayer, setShowLayer] = useState(false);

  return (
    <>
      <FixedHeader
        background="brand"
        pad={{
          horizontal: 'small',
        }}
      >
        <Anchor href="/" icon={<Grid color="white" />} label="ABC Commerce" color="white" />
        <Box
          pad="small"
          justify="center"
          align="center"
          onMouseOver={() => setShowLayer(true)}
          ref={ref}
          border={{
            color: 'brand',
          }}
        >
          <Box direction="row" align="center" gap="small">
            <Heart size="20" color={COLORS.white} />
            <Text color="white">Your wishlist</Text>
          </Box>
          <Box
            animation={{
              type: null,
              duration: 250,
            }}
          >
            <Text color="white">{wishlist && wishlist.count ? wishlist.count : 0}</Text>
          </Box>
        </Box>
        <Box margin="small" direction="row" gap="small">
          <Anchor plain label="Product Center" color="white" href="/manage-product" />
          <Anchor plain label="About" color="white" href="/about" />
        </Box>
      </FixedHeader>
      {showLayer && (
        <Layer
          position="left"
          full="vertical"
          modal={true}
          onEsc={() => {
            setShowLayer(false);
          }}
          onClickOutside={() => {
            setShowLayer(false);
          }}
        >
          <WishLayout wishlist={wishlist} removeWishlist={removeWishList} setShowLayer={setShowLayer} />
        </Layer>
      )}
      {ref.current && (
        <Drop align={{ top: 'bottom' }} target={ref.current} plain>
          <ToolTip
            margin="xsmall"
            pad="small"
            background="brand"
            round
            animation={{
              type: 'fadeOut',
              duration: 4000,
              delay: 1000,
            }}
          >
            <span />
            {toolTipMsg}
          </ToolTip>
        </Drop>
      )}
    </>
  );
}

Header.propTypes = {
  wishlist: PropTypes.shape({
    count: PropTypes.number,
    data: PropTypes.array,
  }),
  toolTipMsg: PropTypes.string,
};

Header.defaultProps = {
  toolTipMsg: 'Your Wishlists are here',
};

export default Header;
