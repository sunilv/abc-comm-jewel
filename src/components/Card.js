import React from 'react';
import { Box, Button, Image } from 'grommet';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { COLORS } from '../styles';

const TagWrapper = styled.div`
  margin-top: 5px;
  display: flex;
  flex-wrap: wrap;
`;

/** Tag  */
const Tag = styled.label`
  border-radius: 5px;
  padding: 5px 15px;
  font-size: 0.7em;
  background: ${(props) => props.bgColor};
  margin: 2px;
  color: ${(props) => props.textColor};
`;

/**
 *
 * @param {Product Information} param0
 * Display Product Info in Card View
 */
const Card = ({
  productId,
  category,
  collection,
  description,
  image,
  addToWishlist,
  isInWishList,
}) => {
  return (
    <Box
      border={{
        color: 'brand',
      }}
      height="400px"
      width="350px"
      gap="small"
      pad="small"
      margin="small"
    >
      <TagWrapper>
        {category ? (
          <Tag bgColor={COLORS.tertiary} textColor="black">
            {category}
          </Tag>
        ) : null}
        {collection ? (
          <Tag bgColor={COLORS.secondary} textColor="black">
            {collection}
          </Tag>
        ) : null}
      </TagWrapper>

      <Box width="500px" height="500px">
        <Image src={image} fit="cover" />
      </Box>
      <Box pad="small" margin="small">
        {description}
      </Box>
      <Box height="small" width="small" justify="center" alignSelf="center">
        <Button
          primary={isInWishList}
          label={isInWishList ? 'In Wishlist' : 'Add to Wishlist'}
          onClick={() => {
            if (!isInWishList) {
              addToWishlist(productId);
            }
          }}
        />
      </Box>
    </Box>
  );
};

Card.propTypes = {
  category: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  collection: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  addToWishlist: PropTypes.func.isRequired,
  isInWishList: PropTypes.bool.isRequired,
};

export default Card;
